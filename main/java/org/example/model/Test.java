package org.example.model;

import java.util.ArrayList;
import java.util.List;

public class Test {
   private String nameTest;

   private Teacher owner;

   private List<Question> listQuestion;

    public Test() {
        listQuestion = new ArrayList<>();
    }

    public String getNameTest() {
        return nameTest;
    }

    public void setNameTest(String nameTest) {
        this.nameTest = nameTest;
    }

    public Teacher getOwner() {
        return owner;
    }

    public void setOwner(Teacher owner) {
        this.owner = owner;
    }

    public List<Question> getListQuestion() {
        return listQuestion;
    }

    public void setListQuestion(List<Question> listQuestion) {
        this.listQuestion = listQuestion;
    }
}
