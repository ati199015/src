package org.example.model;

import java.util.ArrayList;
import java.util.List;

public class Admin implements User {
    private String login;

    private String password;

    private String name;

    private static List<User> userList;

    public Admin() {
        userList = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<User> getUserList() {
        return userList;
    }

    public static void setUserList(List<User> userLists) {
        userList = userLists;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
