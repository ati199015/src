package org.example.model;

public interface User {
    String getName();
    String getLogin();
    String getPassword();
}
