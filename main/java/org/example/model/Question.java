package org.example.model;

import java.util.Scanner;

public class Question {

    private String textQuestion;

    private String variantA;

    private String variantB;
    private String variantC;
    private String variantD;

    private String variantTrue;

    public String getTextQuestion() {
        return textQuestion;
    }

    public void setTextQuestion(String textQuestion) {
        this.textQuestion = textQuestion;
    }

    public String getVariantA() {
        return variantA;
    }

    public void setVariantA(String variantA) {
        this.variantA = variantA;
    }

    public String getVariantB() {
        return variantB;
    }

    public void setVariantB(String variantB) {
        this.variantB = variantB;
    }

    public String getVariantC() {
        return variantC;
    }

    public void setVariantC(String variantC) {
        this.variantC = variantC;
    }

    public String getVariantD() {
        return variantD;
    }

    public void setVariantD(String variantD) {
        this.variantD = variantD;
    }

    public String getVariantTrue() {
        return variantTrue;
    }

    public void setVariantTrue(String variantTrue) {
        this.variantTrue = variantTrue;
    }

   /* public void question() {
    System.out.println("В каком году человек совершил первый полет в космос? \n" +
            "1.1955\n" +
            "2.1961\n" +
            "3.1972");
    Scanner scanner = new Scanner(System.in);
    int answerOption;

    {
        answerOption = scanner.nextInt();
    }
    if(answerOption == 1961){
        System.out.println("Верный ответ");
            }else
                System.out.println("Неверный ответ");
    }*/



}
